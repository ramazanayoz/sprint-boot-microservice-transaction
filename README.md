## Spring Boot Microservice - Transaction Service

### Endpoints

#### 1 -Save Transaction
```
POST /api/transaction HTTP/1.1
Host: localhost:4444
Authorization: Basic basic64
Content-Type: application/json
Content-Length: 40

{
    "userId":1,
    "productId":1
}
```

#### 2- Get Transactions Of User
```
GET /api/transaction/1 HTTP/1.1
Host: localhost:4444
Authorization: Basic basic64
```

#### 3- Delete Transactions By Id
```
DELETE /api/transaction/1 HTTP/1.1
Host: localhost:4444
Authorization: Basic basic64
```

````
http://localhost:4444/h2-console
````
