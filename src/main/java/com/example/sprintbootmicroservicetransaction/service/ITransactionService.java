package com.example.sprintbootmicroservicetransaction.service;

import com.example.sprintbootmicroservicetransaction.model.Transaction;

import java.util.List;

public interface ITransactionService {

    public Transaction saveTransaction(Transaction transaction);

    public void deleteTransaction(Long transactionId);

    public List<Transaction> findAllTransactionsOfUser(Long userId);
}
